#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// ------------------------ examples ------------------------

volatile struct two_words { double a, b; } memory;
  
void handler2(int signum)
{
    printf ("%f, %f\n", memory.a, memory.b);
    alarm (1);
}
   
int example2 (void)
{
    static struct two_words zeros = { 0, 0 }, ones = { 1, 1 };
    signal (SIGALRM, handler2);
    memory = zeros;
    alarm (1);
    while (1)
    {
        memory = zeros;
        memory = ones;
    }
}

void handler1(int signum){
    printf(" signal!\n");
    exit(signum);
}

int example1(void){
    signal(SIGINT, handler1); //TODO obsluga bledow!
    while(1)
    {
        printf("working...\n");
        sleep(1);
    }
    return 0;
}

// ------------------------   code   ------------------------

volatile int flag = 1;

void handler(int signum){
    printf(" signal! - setting flag to 0\n");
    flag = 0;
}

int exercise(void){
    signal(SIGQUIT, handler);
    while(flag)
    {
        printf("working...\n");
        sleep(1);
    }
    return 0;
}

// ------------------------   main   ------------------------

int main(int argc, char const *argv[])
{
	exercise();
	return 0;
}
