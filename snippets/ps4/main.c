#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#define NUM_THREADS 2

// void *PrintHello(void *threadid)
// {
//     long tid;
//     tid = (long)threadid;
//     printf("Hello World! It's me, thread #%ld, I will sleep now!\n", tid);
//     sleep(2);
//     printf("I'm up, exiting thread #%ld...\n", tid);
//     pthread_exit(NULL);
// }

// ======================================== Zadanie 1 ========================================
//aby opcja włączająca optymalizację nie zepsuła programu należy ustawić zmienną jako volatile
volatile long counter;

void *CounterPrint(){
    while (1) {
        printf("%d\n", counter);
        // sleep(1);
        }
}

void *CounterUp(){
    while (1) {
        counter++;
        // printf("counter up\n");
        // sleep(1);
    }
}

void z01(){
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    printf("In main: creating thread %ld\n", t);
    rc = pthread_create(&threads[0], NULL, CounterPrint, (void *)t);
    if (rc)
    {
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        exit(-1);
    }
    rc = pthread_create(&threads[1], NULL, CounterUp, (void *)t);
    if (rc)
    {
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        exit(-1);
    }
    /* Last thing that main() should do */
    pthread_exit(NULL);
    // exit(EXIT_SUCCESS);
}

// ======================================== Zadanie 2 ========================================

volatile long counter;

void *CounterPrint(){
    
}



void z02(){
    
}

// ========================================   MAIN   ========================================

int main(int argc, char *argv[])
{
    z01();



}