#include "fileDaemon.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <utime.h>

// DEBUG SWITCH
int debug = 2;

// Global variables
int theBigLoopFlag = 1;
bool recursive = false;
char srcPath[256], dstPath[256];

/**
 * If the daemon receives a SIGTERM signal, set theBigLoopFlag to 0, which will cause the daemon to exit the main loop and
 * shut down.
 *
 * @param signum The signal number that was received.
 */
void killDaemon(int signum) {
    syslog(LOG_INFO, "Recived signal SIGTERM - shutting down the daemon (signal: %d)", signum);
    theBigLoopFlag = 0;
}

/**
 * It's a signal handler that will be called when the daemon receives a SIGUSR1 signal
 *
 * @param signum The signal number that was received.
 */
void wakeUpDaemon(int signum) {
    syslog(LOG_INFO, "Recived signal SIGUSR1 - forcing synchronization (signal: %d)", signum);
    /* syncFiles(); is not needed - when daemon exits from handler it starts executing from 
       signal(SIGUSR1, wakeUpDaemon);
       declaration.
    */
}

/**
 * It takes two strings as arguments, one of which is a path to the source file and the other is a path to the destination
 * file. It then uses the `stat` function to get the timestamp of the source file and then uses the `utime` function to
 * change the timestamp of the destination file to the one of the source file
 *
 * @param srcFilePath path to the source file
 * @param dstFilePath path to the file that will be changed
 */
void timestampHandler(char srcFilePath[], char dstFilePath[]){
    if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ Attempting to change file timestamp: %s", dstFilePath);

    // variables needed to extract timestamp
    struct stat srcStat;
    struct utimbuf srcTime;

    // extracting timestamp from src file to utime struct
    stat(srcFilePath,&srcStat);
    srcTime.actime = srcStat.st_atime;
    srcTime.modtime = srcStat.st_mtime;

    // changing dst file timestamp
    int code = utime(dstFilePath,&srcTime);
    if (code == 0){
        if(debug > 0) syslog(LOG_DEBUG,"+++++++++++++++ File timestamp changed correctly.");
    } else {
        syslog(LOG_ERR, "=============== File timestamp changing ended with error. File was not changed. (code: %d)", code);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }
}

/**
 * It checks if a file exists
 *
 * @param path The path to the file you want to check.
 *
 * @return True or false
 */
_Bool checkExistence(char path[]) {
    if (access(path, F_OK) == 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * It removes a file
 *
 * @param entityPath The path to the file to be removed.
 */
void removeFile(char entityPath[]) {
    if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ Attempting to remove file: %s", entityPath);
    int code = remove(entityPath);
    if (code == 0) {
        if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ File removed correctly.");
    } else {
        syslog(LOG_ERR, "=============== Removing file ended with error. File was not removed. (code: %d)", code);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }
}

/**
 * It copies a file from source directory to destination directory
 *
 * @param srcEntityPath The path to the source file.
 * @param dstEntityPath The path to the destination file.
 */
void copyFile(char srcEntityPath[], char dstEntityPath[]) {
    if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ Starting to copy file at source path: %s", srcEntityPath);

    // Usunięcie pliku docelowego jeżeli istnieje
    if (checkExistence(dstEntityPath)) {
        if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ Destination file exist, removing...");
        removeFile(dstEntityPath);
    }

    // Zmienne wymagane do kopiowania
    int readSrc;
    int writeDst;
    char tmpPath[256];
    struct stat srcStat;
    struct stat dstStat;
    off_t offset = 0;
    strcpy(tmpPath, "");
    strcat(tmpPath,dstEntityPath);
    strcat(tmpPath,".tmp");

    // Otwieranie stumienia czytającego
    if ((readSrc = open(srcEntityPath, O_RDONLY)) == -1) {
        syslog(LOG_ERR, "====== File does not exist or it can't be read. Path: %s", srcEntityPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }

    // Tworzenie pliku i/lub otwieranie strumienia zapisującego
    if ((writeDst = open(tmpPath, O_WRONLY | O_CREAT, srcStat.st_mode)) == -1) {
        syslog(LOG_ERR, "====== File can't be created or it can't be writed. Path: %s", tmpPath);
        syslog(LOG_ERR, "Exiting...");
        remove(tmpPath);
        exit(EXIT_FAILURE);
    }

    fstat(readSrc, &srcStat);

    // Kopiowanie danych
    if ((sendfile(writeDst, readSrc, &offset, srcStat.st_size)) != -1) {
        if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ Temporay file coping finished successful.");
        close(readSrc);
        close(writeDst);

        timestampHandler(srcEntityPath,tmpPath);

        // Sprawdzanie zgodności
        stat(tmpPath,&dstStat);
        if(srcStat.st_size == dstStat.st_size && srcStat.st_blksize == dstStat.st_blksize && srcStat.st_blocks == dstStat.st_blocks){
            if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ File copy is correct. Size : %ld BlockSize : %ld Blocks %ld",dstStat.st_size,dstStat.st_blksize,dstStat.st_blocks);
            rename(tmpPath,dstEntityPath);
            chmod(dstEntityPath, srcStat.st_mode);
            if (debug > 0) syslog(LOG_DEBUG, "+++++++++++++++ File coping finished successful.");
        } else {
            remove(tmpPath);
            syslog(LOG_ERR, "====== File copping occurred error.");
            syslog(LOG_ERR, "Exiting...");
            exit(EXIT_FAILURE);
        }
    } else {
        syslog(LOG_ERR, "====== Temporary file copping occurred error.");
        syslog(LOG_ERR, "Exiting...");
        remove(tmpPath);
        exit(EXIT_FAILURE);
        close(readSrc);
        close(writeDst);
    }
}

/**
 * It creates a directory in the destination directory with the same name as the source directory
 *
 * @param srcEntityPath path to the source entity
 * @param dstEntityPath The path to the destination file.
 */
void createDirectory(char srcEntityPath[], char dstEntityPath[]) {
    if (debug > 0) syslog(LOG_DEBUG, "++++++ Attempting to create directory.");

    struct stat srcStat;
    stat(srcEntityPath, &srcStat);
    // Tworzenie katalogu
    switch (mkdir(dstEntityPath, srcStat.st_mode)) {
        case 0:
            if (debug > 0) syslog(LOG_DEBUG, "++++++ Directory created correctly.");
            break;
        case -1:
            syslog(LOG_ERR, "====== Creating directory ended with error. Directory was not created.");
            syslog(LOG_ERR, "Exiting...");
            exit(EXIT_FAILURE);
            break;
        default:
            syslog(LOG_ERR, "====== Unexpected error while creating directory.");
            syslog(LOG_ERR, "Exiting...");
            exit(EXIT_FAILURE);
            break;
    }
}

/**
 * It removes a directory and all its content
 *
 * @param dirPath path to the directory to be removed
 *
 * @return 0
 */
int removeDirectory(char dirPath[]) {
    if (debug > 0) syslog(LOG_DEBUG, "++++++ Starting to remove directory at path: %s", dirPath);

    // Otwarcie dir do usunięcia
    struct dirent* dirEntity;
    DIR* dir = opendir(dirPath);
    if (dir == NULL) {
        syslog(LOG_ERR, "====== Directory path does not exist or could not be read. Path: %s", dirPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }
    if (debug > 0) syslog(LOG_DEBUG, "++++++ Directory path correct. Path: %s", dirPath);

    // Zmienne do użycia w pętli
    char entityPath[256] = {0};

    // Pętla iterująca po katalogu
    while (dirEntity = readdir(dir)) {
        
        if (strcmp(dirEntity->d_name, ".") != 0 && strcmp(dirEntity->d_name, "..") != 0) {
            // Ustawienie ścieżki dla elementu
            strcpy(entityPath, "");
            strcat(entityPath, dirPath);
            strcat(entityPath, "/");
            strcat(entityPath, dirEntity->d_name);

            if (debug > 0) syslog(LOG_DEBUG, "DEBUG %s %s %d", entityPath, dirEntity->d_name, dirEntity->d_type);

            // Sprawdzanie typu elementu i usunięcie lub ewentualne usuwanie podfolderu
            switch (dirEntity->d_type) {
                case DT_DIR:
                    // statements
                    removeDirectory(entityPath);
                    syslog(LOG_INFO, "====== Removing content of directory: %s", entityPath);
                    break;
                case DT_REG:
                    // statements
                    removeFile(entityPath);
                    syslog(LOG_INFO, "====== File %s has been removed.", entityPath);
                    break;
                default:
                    syslog(LOG_ERR, "====== Entity %s cannot be handled or it is unsupported.", entityPath);
            }
        }
    }
    closedir(dir);
    int code = rmdir(dirPath);

    if (code == 0) {
        syslog(LOG_INFO, "====== Directory removal is successful.");
    } else {
        syslog(LOG_ERR, "====== Error occured during directory removal. ");
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }
    return 0;
}

/**
 * It iterates through the source directory and checks if the file exists in the destination directory. If it does, it
 * checks if the file has been modified. If it hasn't, it does nothing. If it has, it copies the file. If the file doesn't
 * exist, it copies it
 *
 * @param srcPath path to the source directory
 * @param dstPath path to the destination directory
 */
void copyHandler(char srcPath[], char dstPath[]) {
    // Otwarcie srcDir
    struct dirent* srcEntity;
    DIR* srcDir = opendir(srcPath);
    if (srcDir == NULL) {
        syslog(LOG_ERR, "========= Source path does not exist or could not be read. Path: %s", srcPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }

    // Otwarcie dstDir
    struct dirent* dstEntity;
    DIR* dstDir = opendir(dstPath);
    if (dstDir == NULL) {
        syslog(LOG_ERR, "========= Destination path does not exist or could not be read. Path: %s", dstPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }

    syslog(LOG_INFO, "====== SRC:OK; DST:OK; Looking for entities to copy to path: %s", dstPath);

    // zmienne do wykorzystania w pętli
    bool dstEntityExists = false;
    char srcEntityPath[256] = {0}, dstEntityPath[256] = {0};
    struct stat srcEntityStats, dstEntityStats;

    // pętla iterująca po obu katalogach
    while (srcEntity = readdir(srcDir)) {
        // ustawienie path srcEntity
        strcpy(srcEntityPath, "");
        strcat(srcEntityPath, srcPath);
        strcat(srcEntityPath, "/");
        strcat(srcEntityPath, srcEntity->d_name);

        // Pętla sprawdzająca pliki do skopiowania
        if (strcmp(srcEntity->d_name, ".") != 0 && strcmp(srcEntity->d_name, "..") != 0) {
            dstEntityExists = false;
            rewinddir(dstDir);

            while (dstEntity = readdir(dstDir)) {
                if (debug > 1) syslog(LOG_DEBUG, "DEBUG BEFORE srcEntity->d_name: %s ;dstEntity->d_name: %s", srcEntity->d_name, dstEntity->d_name);
                if (strcmp(srcEntity->d_name, dstEntity->d_name) == 0) {
                    if (debug > 1) syslog(LOG_DEBUG, "++++++++++++ Found entity, checking if modified... (file: %s)", srcEntityPath);
                    dstEntityExists = true;

                    // ustawienie path dstEntity
                    strcpy(dstEntityPath, "");
                    strcat(dstEntityPath, dstPath);
                    strcat(dstEntityPath, "/");
                    strcat(dstEntityPath, dstEntity->d_name);

                    // ustawienie handlera do pobierania statystyk plików
                    stat(srcEntityPath, &srcEntityStats);
                    stat(dstEntityPath, &dstEntityStats);

                    // sprawdzanie czy entity jest katalogiem do rekurencji
                    if (debug > 0) syslog(LOG_DEBUG, "DEBUG srcType: %d src: %s ; dst: %s", srcEntity->d_type, srcEntityPath, dstEntityPath);
                    if (srcEntity->d_type == DT_DIR && recursive == true) {
                        syslog(LOG_INFO, "============ Found modified directory, checking contents... (file: %s)", srcEntityPath);
                        copyHandler(srcEntityPath, dstEntityPath);
                        syslog(LOG_INFO, "============ Exiting directory (file: %s)", srcEntityPath);
                    } else if (srcEntity->d_type == DT_REG && srcEntityStats.st_mtime > dstEntityStats.st_mtime) {
                        syslog(LOG_INFO, "============ Found modified file, copying... (file: %s)", srcEntityPath);
                        syslog(LOG_INFO, "============ Source date: %ld - Destination date: %ld", srcEntityStats.st_mtime, dstEntityStats.st_mtime);
                        copyFile(srcEntityPath, dstEntityPath);
                    }
                }
                if (debug > 1) syslog(LOG_DEBUG, "DEBUG AFTER  srcEntity->d_name: %s ;dstEntity->d_name: %s", srcEntity->d_name, dstEntity->d_name);
            }
            // if (debug > 0) syslog(LOG_DEBUG, "%s %s %d", srcEntityPath, dstEntityPath, dstEntityExists);
            if (dstEntityExists == false) {
                // ustawienie path dstEntity (nazwa pliku jest taka sama więc można użyć srcEntity->d_name)
                strcpy(dstEntityPath, "");
                strcat(dstEntityPath, dstPath);
                strcat(dstEntityPath, "/");
                strcat(dstEntityPath, srcEntity->d_name);

                // sprawdzanie czy entity jest katalogiem do rekurencji
                if (srcEntity->d_type == DT_DIR && recursive == true) {
                    syslog(LOG_INFO, "============ Found new directory, copying... (file: %s)", srcEntityPath);
                    createDirectory(srcEntityPath, dstEntityPath);
                    copyHandler(srcEntityPath, dstEntityPath);
                    // Kopiowanie całego katalogu? czy tworzenie nowego katalogu w dst i kopiowanie plik po pliku?
                } else if (srcEntity->d_type == DT_REG) {
                    syslog(LOG_INFO, "============ Found new file, copying... (file: %s)", srcEntityPath);
                    copyFile(srcEntityPath, dstEntityPath);
                }
            }
        }
    }

    closedir(srcDir);
    closedir(dstDir);
}

/**
 * It iterates through the destination directory and checks if the file exists in the source directory. If it doesn't, it
 * removes it
 *
 * @param srcPath path to the source directory
 * @param dstPath path to the destination directory
 */
void removeHandler(char srcPath[], char dstPath[]) {
    // Otwarcie dstDir
    struct dirent* dstEntity;
    DIR* dstDir = opendir(dstPath);
    if (dstDir == NULL) {
        syslog(LOG_ERR, "========= Destination path does not exist or could not be read. Path: %s", dstPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }

    // Otwarcie srcDir
    struct dirent* srcEntity;
    DIR* srcDir = opendir(srcPath);
    if (srcDir == NULL) {
        syslog(LOG_ERR, "========= Source path does not exist or could not be read. Path: %s", srcPath);
        syslog(LOG_ERR, "Exiting...");
        exit(EXIT_FAILURE);
    }

    syslog(LOG_INFO, "====== SRC:OK; DST:OK; Looking for entities to remove in path: %s", dstPath);

    // zmienne do wykorzystania w pętli
    bool srcEntityExists = false;
    char srcEntityPath[256] = {0}, dstEntityPath[256] = {0};

    // pętla iterująca po obu katalogach
    while (dstEntity = readdir(dstDir)) {
        // ustawienie path srcEntity
        strcpy(dstEntityPath, "");
        strcat(dstEntityPath, dstPath);
        strcat(dstEntityPath, "/");
        strcat(dstEntityPath, dstEntity->d_name);

        // Pętla sprawdzająca pliki do usunięcia
        if (strcmp(dstEntity->d_name, ".") != 0 & strcmp(dstEntity->d_name, "..") != 0) {
            srcEntityExists = false;
            rewinddir(srcDir);

            while (srcEntity = readdir(srcDir)) {
                if (debug > 1) syslog(LOG_DEBUG, "DEBUG BEFORE srcEntity->d_name: %s ;dstEntity->d_name: %s", srcEntity->d_name, dstEntity->d_name);
                if (strcmp(dstEntity->d_name, srcEntity->d_name) == 0) {
                    if (debug > 1) syslog(LOG_DEBUG, "++++++++++++ Found entity (file: %s)", srcEntityPath);
                    srcEntityExists = true;

                    // ustawienie path srcEntity
                    strcpy(srcEntityPath, "");
                    strcat(srcEntityPath, srcPath);
                    strcat(srcEntityPath, "/");
                    strcat(srcEntityPath, srcEntity->d_name);

                    // sprawdzanie czy entity jest katalogiem do rekurencji
                    if (debug > 0) syslog(LOG_DEBUG, "DEBUG srcType: %d src: %s ; dst: %s", srcEntity->d_type, srcEntityPath, dstEntityPath);
                    if (srcEntity->d_type == DT_DIR && recursive == true) {
                        syslog(LOG_INFO, "============ Found directory, checking contents... (file: %s)", srcEntityPath);
                        removeHandler(srcEntityPath, dstEntityPath);
                        syslog(LOG_INFO, "============ Exiting directory (file: %s)", srcEntityPath);
                    } else if (srcEntity->d_type == DT_REG) {
                        break;
                    }
                }
                if (debug > 1) syslog(LOG_DEBUG, "DEBUG AFTER  srcEntity->d_name: %s ;dstEntity->d_name: %s", srcEntity->d_name, dstEntity->d_name);
            }
            if (srcEntityExists == false) {
                // sprawdzanie czy entity jest katalogiem do rekurencji
                if (dstEntity->d_type == DT_DIR && recursive == true) {
                    syslog(LOG_INFO, "============ Found directory not included in source directory, removing... (file: %s)", dstEntityPath);
                    removeDirectory(dstEntityPath);
                } else if (dstEntity->d_type == DT_REG) {
                    syslog(LOG_INFO, "============ Found file not included in source directory, removing... (file: %s)", dstEntityPath);
                    removeFile(dstEntityPath);
                }
            }
        }
    }
    closedir(srcDir);
    closedir(dstDir);
}

/**
 * It copies files from the source directory to the destination directory, and then removes files from the destination
 * directory that are not in the source directory
 */
void syncFiles() {
    syslog(LOG_INFO, "=== Waking up, starting synchronization");

    copyHandler(srcPath, dstPath);
    removeHandler(srcPath, dstPath);

    syslog(LOG_INFO, "=== Files synchronized, going back to sleep");
}

/**
 * It forks a child process, sets the child process as a daemon, and then calls the syncFiles() function every waitTime
 * seconds
 *
 * @param srcPathIn The source path to sync from.
 * @param dstPathIn The destination path for the files to be copied to.
 * @param recursiveIn 1 if you want to sync subdirectories, 0 if you don't.
 * @param waitTime The time in seconds between each sync.
 */
void fileDaemon(char srcPathIn[], char dstPathIn[], int recursiveIn, unsigned long waitTime) {
    strncpy(srcPath, srcPathIn, 256);
    strncpy(dstPath, dstPathIn, 256);
    recursive = recursiveIn;

    /* Our process ID and Session ID */
    pid_t pid, sid;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    /* Change the file mode mask */
    umask(0);

    /* Open any logs here */
    openlog("fileSyncDaemon", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_DAEMON);
    syslog(LOG_INFO, "Daemon started with arguments - srcPath:%s; dstPath:%s; recursive:%d; waitTime:%lu", srcPath, dstPath, recursive, waitTime);

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        /* Log the failure */
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory */
    if ((chdir("/")) < 0) {
        /* Log the failure */
        exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    /* Daemon-specific initialization goes here */
    signal(SIGUSR1, wakeUpDaemon);
    signal(SIGTERM, killDaemon);

    /* The Big Loop */
    while (theBigLoopFlag) {
        syncFiles();
        sleep(waitTime);
    }

    syslog(LOG_INFO, "Closing log.");
    closelog();
    exit(EXIT_SUCCESS);
}
