#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#include "fileDaemon.h"

/**
 * It prints the help message and exits the program
 *
 * @param code
 */
void printHelp(int code) {
    if (code == 1) printf("Wrong usage - not enough arguments\n\n");
    printf("Usage: fileSyncDaemon <SOURCE> <DESTINATION> [OPTION 1] [OPTION 2]\n");
    printf("OPTIONS: \n");
    printf("  [number > 0]				set custom wait time\n");
    printf("  -r, -R				check directories recursively\n");
    if (code == 1) {
        syslog(LOG_ERR, "Wrong usage - not enough arguments");
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}

/**
 * It takes the arguments from the command line, checks if they are correct, and if they are, it calls the fileDaemon
 * function
 *
 * @param argc number of arguments
 * @param argv
 */
int main(int argc, char const *argv[]) {
    openlog("fileSyncDaemonCaller", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_DAEMON);

    if (argc == 2 && strcmp(argv[1], "-h") == 0 || argc == 2 && strcmp(argv[1], "--help") == 0) printHelp(0);
    if (argc < 3) {
        printHelp(1);
    }

    bool recursive = false;
    unsigned long waitTime = 300;
    char src[256], dst[256];

    // copying directory paths
    strncpy(src, argv[1], 256);
    strncpy(dst, argv[2], 256);

    // truncating last slash for compatibility with autocomplete
    int srcSize = strlen(src), dstSize = strlen(dst);
    if (src[srcSize - 1] == '/') src[srcSize - 1] = '\0';
    if (dst[dstSize - 1] == '/') dst[dstSize - 1] = '\0';

    int count = 3;
    while (count < argc) {
        if (count > 4) {
            printf("Wrong usage - too many arguments\n");
            syslog(LOG_ERR, "Wrong usage - too many arguments");
            exit(EXIT_FAILURE);
        }

        char *argLongFollow;
        unsigned long argLong = strtoul(argv[count], &argLongFollow, 10);
        int checkArgRecursiveUpper = strncmp(argv[count], "-R", 2);
        int checkArgRecursiveLower = strncmp(argv[count], "-r", 2);
        int checkArgLongFollow = strncmp(argLongFollow, "", 1);

		// debugging syslogs
		// syslog(LOG_DEBUG, "checkArgRecursiveUpper for count %d: %d\n", count, checkArgRecursiveUpper);
		// syslog(LOG_DEBUG, "checkArgRecursiveLower for count %d: %d\n", count, checkArgRecursiveLower);
		// syslog(LOG_DEBUG, "argLong for count %d: %lu\n", count, argLong);
		// syslog(LOG_DEBUG, "argLongFollow for count %d: %s\n", count, argLongFollow);
		// syslog(LOG_DEBUG, "checkArgLongFollow for count %d: %d\n", count, checkArgLongFollow);
	

		// handling options -r, -R and [waitTime]
        if (checkArgRecursiveUpper == 0 || checkArgRecursiveLower == 0)
            recursive = true;
        if (argLong > 0 && checkArgLongFollow == 0)
            waitTime = argLong;
        if (checkArgRecursiveUpper != 0 && checkArgRecursiveLower != 0 && argLong <= 0 || checkArgRecursiveUpper != 0 && checkArgRecursiveLower != 0 && checkArgLongFollow != 0) {
            printf("wrong argument: %s\n", argv[count]);
            syslog(LOG_ERR, "Wrong usage - provided forbidden argument: %s", argv[count]);
            exit(EXIT_FAILURE);
        }
        count++;
    }

    syslog(LOG_INFO, "Input parameters - argv[1]: %s; argv[2]: %s; argv[3]: %s; argv[4]: %s; ", argv[1], argv[2], argv[3], argv[4]);
    syslog(LOG_INFO, "Daemon starting with arguments - src:%s; dst:%s; recursive:%d; waitTime:%lu", src, dst, recursive, waitTime);
    closelog();

    fileDaemon(src, dst, recursive, waitTime);
}